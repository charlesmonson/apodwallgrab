def get_apod_info(date=''):
    if date:
        r = requests.get(f'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date={date}')
    else:
        r = requests.get(f'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY')
    js = json.loads(r.content)
    if js["media_type"] == 'image':
        return js
    else:
        return False

def save_apod_image(info):
    r = requests.get(info["hdurl"])
    with open('apod.jpg', 'wb') as outfile:
        outfile.write(r.content)

def save_apod_info(info):
    with open('apod-info.txt', 'w') as outfile:
        outfile.write(f'{info["date"]}\n\n{info["title"]}\n\n{info["explanation"]}\n')

def main():
    apod = get_apod_info()
    if apod:
        save_apod_info(apod)
        save_apod_image(apod)

if __name__ == "__main__":
    main()
